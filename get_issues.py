
'''
This script will pull all the issues and their comments from a
Bitbucket repo using the Bitbucket API. The issue *must* be 
*public* for this script to work.

'''

__author__ = 'Varun Godbole'
__email__ = 'godbolevarun@gmail.com'

import sys
import os
import requests
import json
import time
import codecs

verbose = True

def usage():
    print 'Usage:', sys.argv[0], 'repo', '[target]'
    print '    repo    The reference to the repo in the form "owner/name"'
    print '    target  The target folder for issues to be written in (defaults to the working dir)'

def dash(n):
    return '-'*n
    
def write(f, s):
    f.write(s + '\n')

def extract_time(tup):
    s = tup[0]
    return time.mktime(time.strptime(s, "%Y-%m-%d %H:%M:%S+00:00"))

def parse_issue(curr_issue):
    title = '#%d - %s' % (curr_issue['local_id'], curr_issue['title'])
    
    if verbose: print '  '+(title[:50])+'...',
    
    created_on = curr_issue['created_on']
    status = curr_issue['status']
    issue_type = curr_issue['metadata']['kind']
    reported_by = str(curr_issue['reported_by']['first_name']) + ' ' + str(curr_issue['reported_by']['last_name'])
    responsible = None
    if 'responsible' in curr_issue:
        responsible = str(curr_issue['responsible']['first_name']) + ' ' + str(curr_issue['responsible']['last_name'])
    issue_contents = curr_issue['content']
    
    comment_count = int(curr_issue['comment_count'])
    issue_id = curr_issue['local_id']
    comments_url = url + '/' + str(issue_id) + '/comments'
    comments_resp = requests.get(comments_url)

    f = codecs.open(os.path.join(target, 'Issue '+str(issue_id)), 'a', 'utf-8')
    write(f, 'Title: ' + title)
    write(f, 'Created on: ' + created_on)
    write(f, 'Status: ' + status)
    write(f, 'Reported by: ' + reported_by)
    if responsible:
        write(f, 'Responsible: ' + responsible)
    write(f, 'Comment: ')
    write(f, issue_contents)
    write(f, '')
    write(f, dash(4))
    
    num_comments = len(comments_resp.json)
    comments = []
    
    for j in range(num_comments):
        curr_comment = comments_resp.json[j]
        commenter = str(curr_comment['author_info']['first_name']) + ' ' + str(curr_comment['author_info']['last_name'])
        comment_created = curr_comment['utc_created_on']
        comment_updated = curr_comment['utc_updated_on']
        comment_content = curr_comment['content']
        
        if not comment_content:
            continue
        
        tup = (comment_created, comment_updated, commenter, comment_content)
        comments.append(tup)    
    
    sorted_comments = sorted(comments, key=extract_time)
    for el in sorted_comments:
        (comment_created, comment_updated, commenter, comment_content) = el
        write(f, 'For Issue: ' + title)
        write(f, 'Comment by: ' + commenter)
        write(f, 'Comment Created: ' + comment_created)
        write(f, 'Comment Updated: ' + comment_updated)
        write(f, 'Comment: ')
        write(f, comment_content)
        write(f, dash(4))
        write(f, '')
        
    write(f, dash(10))
    write(f, '')
    f.close()
    
    if verbose: print 'done'
        
def parse_response(r):
    for iss in r.json['issues']:
        parse_issue(iss)
    
if __name__ == '__main__': 
    if len(sys.argv) is 1:
        usage();
        exit();

    url = 'https://api.bitbucket.org/1.0/repositories/'+sys.argv[1]+'/issues'
    target = ''
    if len(sys.argv) >= 3: target = sys.argv[2]
    
    offset = 0
    limit = 50
    count = 9000
    i = 0
    while i < count:
        if verbose: print 'Fetching %d issues starting at %d...' % (limit, offset),
        r = requests.get(url+('?sort=local_id&limit=%d&start=%d' % (limit, offset)))
        if verbose: print 'done'
        parse_response(r)
        count = r.json['count']
        i += limit
        offset += limit
    
